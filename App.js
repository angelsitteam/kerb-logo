import React from 'react';
import {StyleSheet, Slider, View, Button} from 'react-native';
import Logo from './components/Logo';



export default class App extends React.Component {


    state = {
        invertedLogo: false,
        sizeLogo:150
    };

    handleChangeSize = (value) => {
        this.setState((prevState, props) => {
            return {sizeLogo:value}
        },()=>this.refs.logo._playAnimation());
    };

    handleInvert = () => {
        this.setState((prevState, props) => {
            return {invertedLogo: !prevState.invertedLogo}
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerTop}>
                    <Logo ref="logo" invert={this.state.invertedLogo} size={this.state.sizeLogo} />
                </View>
                <View style={[styles.containerBottom]}>

                    <Slider  onValueChange={this.handleChangeSize} style={{paddingVertical:20,width:'90%'}}  maximumValue = {300} minimumValue ={50} step={10} value={this.state.sizeLogo}  />

                    <View style={styles.buttonsGroup}>

                    <Button style={styles.button}  onPress={() => {
                        this.refs.logo._playAnimation();
                        console.log('pressed')
                    }} title={'restart'}/>

                    <Button style={styles.button} onPress={this.handleInvert} title={'invert'}/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
    containerTop: {
        flex: 4,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerBottom: {
        flex: 1,
        flexWrap:'wrap',
        flexDirection: 'column',
        backgroundColor: '#ddd',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonsGroup: {
        flex: 1,
        width:'90%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
   button: {
   }

});
