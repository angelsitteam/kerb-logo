import React from "react";
import { StyleSheet, View } from "react-native";
import { DangerZone } from "expo";

let { Lottie } = DangerZone;

export default class Logo extends React.PureComponent {
  componentDidMount() {
    this._playAnimation();
  }

  render() {
    const { invert, size } = this.props,
      source = invert
        ? require("./logo-black.json")
        : require("./logo-white.json");
    return (
      <View
        style={[
          styles.animationContainer,
          { backgroundColor: invert ? "#fff" : "#000" }
        ]}
      >
        {this.state.animation && (
          <Lottie
            loop
            ref={animation => {
              this.animation = animation;
            }}
            style={{
              width: size,
              height: size,
              backgroundColor: invert ? "#fff" : "#000"
            }}
            source={source}
          />
        )}
      </View>
    );
  }

  _playAnimation = () => {
    console.log("_playAnimation");
    if (this.animation) {
      this.animation.reset();
      this.animation.play();
    }
  };
}

const styles = StyleSheet.create({
  animationContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    width: "100%"
  }
});
